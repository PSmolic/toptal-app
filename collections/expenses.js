Expenses = new Meteor.Collection('expenses');

Expenses.allow({
  insert() {
    return false;
  },
  update() {
    return false;
  },
  remove() {
    return false;
  }
});

Expenses.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  }
});

ExpensesSchema = new SimpleSchema({
  "name": {
    type: String,
    label: "Expense Name"
  },
  "_userId": {
    type: String,
    label: "Owner Id"
  },
  "description": {
    type: String,
    label: "Expense description"
  },
  "amount": {
    type: Number,
    label: "Expense amount"
  },
  "comment": {
    type: String,
    label: "Expense comment"
  },
  "date": {
    type: Date
  },
  "created": {
    type: Date,
    denyUpdate: true
  },
  "updated": {
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  }

});

Expenses.attachSchema(ExpensesSchema);