Template.registerHelper( 'isCurrentUser', ( currentUser ) => {
  return currentUser === Meteor.userId() ? true : false;
});

Template.registerHelper( 'disableIfAdmin', ( userId ) => {
  if ( Meteor.userId() === userId ) {
    return Roles.userIsInRole( userId, ['admin', 'manager'] ) ? "disabled" : "";
  }
});

Template.registerHelper( 'disableAdminsForManagers', ( userId ) => {
  if ( Roles.userIsInRole( Meteor.userId(), 'manager' )) {
    return Roles.userIsInRole( userId, 'admin' ) ? "disabled" : "";
  }
});

Template.registerHelper( 'selected', ( v1, v2 ) => {
  return v1 === v2 ? true : false;
});

Template.registerHelper( 'selectedUser', ( v1 ) => {
  return v1 === Session.get('selectedUserId') ? true : false;
});

Template.registerHelper('formatDate', function(date) {
  return moment(date).tz("Europe/London").format('MM/DD/YYYY h:mm A');
});

Template.registerHelper('pages', function(numPages) {
  var pages = [];
  for(var i = 1; i <= numPages; i++) {
    pages.push({'page': i});
  }

  return pages;
});

Template.registerHelper('activePage', function(page1, page2) {
  return (page1 == page2) ? 'active' : '';
});

Template.registerHelper('class', function(loaded) {
  return (loaded) ? '' : 'hide';
});

