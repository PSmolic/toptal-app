Template.index.onCreated( () => {
  Template.instance().subscribe( 'template' );
  Template.instance().subscribe( 'expenses' );
});
