Template.editExpense.helpers({
  expense: function() {

    return Expenses.findOne({
      _id: FlowRouter.getParam('id')
    });
  }
});

Template.editExpense.onRendered(() => {
  Modules.client.editExpense({
    form: "#edit-expense",
    template: Template.instance()
  });

  $('.datepicker-edit').datetimepicker();
});

Template.editExpense.events({
  'submit form': (event) => event.preventDefault(),
  'click .delete': function() {
    if ( confirm( "Are you sure? This is permanent." ) ) {
      Meteor.call( "removeExpense", FlowRouter.getParam('id'), function( error, response ) {
        if ( error ) {
          Bert.alert( error.reason, "warning" );
        } else {
          FlowRouter.go( 'expenses' );
          Bert.alert( "Expense removed!", "success" );
        }
      });
    }
  }
});
