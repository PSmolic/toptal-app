Meteor.subscribe('expenses');
Meteor.subscribe( 'users' );

Template.expensesList.helpers({
  expenses: function() {

    var limit = parseInt(FlowRouter.getQueryParam('limit')) || 5;
    var searchRegex = new RegExp(Session.get('search'), 'i');

    var amount = {};
    if(Session.get('amountLessThan')) {
      amount['$lte'] = Session.get('amountLessThan');
    }
    if(Session.get('amountMoreThan')) {
      amount['$gte'] = Session.get('amountMoreThan');
    }

    var findObject = {$or: [{name:{$regex:searchRegex}}, {description:{$regex:searchRegex}}]};

    if(Object.keys(amount).length) {
      findObject['amount'] = amount;
    }

    if(Session.get('selectedUserId') && Roles.userIsInRole( Meteor.userId(), 'admin' )) {
      findObject['_userId'] = Session.get('selectedUserId');
    }
    else {
      //findObject['_userId'] =  Meteor.userId();
    }

    var countExpenses = parseInt(Expenses.find(findObject).count()) || 0;

    Session.set("limit", limit);
    Session.set("countExpenses", countExpenses);
    Session.set("numPages",Math.ceil(Session.get("countExpenses") / Session.get("limit")));

    var page = Math.min(parseInt(FlowRouter.getQueryParam('page')), Session.get("numPages"));

    var offset = (page - 1) * limit;
    Session.set("offset", offset);

    var pagination = {
      limit: limit,
      skip: offset
    }

    return Expenses.find(findObject, pagination);
  },
  pathForExpense: function() {
    var expense = this;
    var params = {
      id: expense._id
    };
    var routeName = "editExpense";
    var path = FlowRouter.path(routeName, params, {page: Session.get("currPage")});

    return path;
  },
  currPage: function() {
    Session.set("currPage", (Session.get("offset") / Session.get("limit")) + 1);
    return  Session.get("currPage");
  },
  numPages: function() {
    return Session.get("numPages");
  },
  search: function() {
    return Session.get('search');
  },
  users: function() {
    var users = Meteor.users.find();

    if ( users ) {
      return users;
    }
  },
  amountLessThan: function() {
    return Session.get('amountLessThan');
  },
  amountMoreThan: function() {
    return Session.get('amountMoreThan');
  }
});

Template.expensesList.onRendered(function () {

    if(!Session.get('selectedUserId') && $('[name="usersList"]').val()) {
      var selectedUserId = $('[name="usersList"]').val();

      Session.set('selectedUserId', selectedUserId);
    }
});

Template.expensesList.events = {
  'click #search': function(evt, template){
      Session.set('search', template.find('#search-box').value);
  },
  'input .amountLessThan': function( event, template ) {
    Session.set('amountLessThan', parseInt(template.find('.amountLessThan').value) || '');
  },
  'input .amountMoreThan': function( event, template ) {
    Session.set('amountMoreThan', parseInt(template.find('.amountMoreThan').value) || '');
  },
  'change [name="usersList"]': function( event, template ) {
    var selectedUserId = $( event.target ).find( 'option:selected' ).val();

    Session.set('selectedUserId', selectedUserId);
  },
};
