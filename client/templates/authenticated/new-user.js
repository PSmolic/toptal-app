Template.newUser.helpers({
});

Template.newUser.onRendered(() => {
  Modules.client.newUser({
    form: "#new-user",
    template: Template.instance()
  });
});

Template.newUser.events({
  'submit form': (event) => event.preventDefault(),
});
