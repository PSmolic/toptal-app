Meteor.subscribe( 'users' );

Template.users.events({
  'change [name="userRole"]': function( event, template ) {
    let role = $( event.target ).find( 'option:selected' ).val();

    Meteor.call( "setRoleOnUser", {
      user: this._id,
      role: role
    }, ( error, response ) => {
      if ( error ) {
        Bert.alert( error.reason, "warning" );
      }
    });
  },
});

Template.users.helpers({
  users: function() {
    var users = Meteor.users.find();

    if ( users ) {
      return users;
    }
  },
  pathForUser: function() {
    var user = this;
    var params = {
        id: user._id
    };
    var routeName = "editUsers";
    var path = FlowRouter.path(routeName, params);

    return path;
  },
  disableIfManager: function() {
    return Roles.userIsInRole( Meteor.userId(), ['manager'] ) ? "disabled" : "";
  }
});