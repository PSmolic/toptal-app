Template.editUser.helpers({
  user: function() {

    return Meteor.users.findOne({
      _id: FlowRouter.getParam('id')
    });
  }
});

Template.editUser.onRendered(() => {
  Modules.client.editUser({
    form: "#edit-user",
    template: Template.instance()
  });
});

Template.editUser.events({
  'submit form': (event) => event.preventDefault(),
  'click .delete': function() {
    if ( confirm( "Are you sure? This is permanent." ) ) {
      Meteor.call( "removeUser", FlowRouter.getParam('id'), function( error, response ) {
        if ( error ) {
          Bert.alert( error.reason, "warning" );
        } else {
          FlowRouter.go( 'users' );
          Bert.alert( "Expense removed!", "success" );
        }
      });
    }
  }
});
