Meteor.subscribe( 'users' );
Meteor.subscribe( 'expenses' );

Template.expensesPerWeek.events({
  'change [name="weeks"]': function( event, template ) {
    var selectedWeekNum = parseInt($( event.target ).find( 'option:selected' ).val());

    Session.set('weekNum', selectedWeekNum);
  },
  'click .printExpenses': function() {
    window.print();
  }
});

Template.expensesPerWeek.helpers({
  users: function() {
    var users = Meteor.users.find();

    if ( users ) {
      return users;
    }
  },
  weeks: function() {
    var expenses = ReactiveMethod.call('groupedByWeek');

    if(expenses) {
      expenses.sort(function(a, b) {
          return a._id - b._id;
      });

      Session.set('weekNum', expenses[0]._id);
      Session.set('expenses', expenses);
    }

    return expenses;
  },
  expensesByDay: function() {

    var expenses = ReactiveMethod.call('groupedByDay', parseInt(Session.get('weekNum')));

    if(expenses) {
      expenses.sort(function(a, b) {
          return a._id - b._id;
      });

      return expenses;
    }
  },
  totalForWeek: function() {
    var expenses = Session.get('expenses');

    if(expenses) {
      var obj = $.grep(Session.get('expenses'), function(e){ return e._id == Session.get('weekNum'); });

      return obj[0].totalAmount;
    }
  }

});
