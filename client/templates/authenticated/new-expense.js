Template.newExpense.helpers({

});

Template.newExpense.onRendered(() => {
  Modules.client.newExpense({
    form: "#new-expense",
    template: Template.instance()
  });

  $('.datepicker').datetimepicker();
});

Template.newExpense.events({
  'submit form': (event) => event.preventDefault(),
});
