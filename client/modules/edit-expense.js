let editExpense = ( options ) => {
  _validate( options.form, options.template );
};

let _validate = ( form, template ) => {
  $( form ).validate( validation( template ) );
};

let validation = ( template ) => {
  return {
    rules: {
      name: {
        required: true,
        minlength: 3
      },
      amount: {
        required: true,
        minlength: 1,
        number: true
      },
      description: {
        required: true
      },
      comment: {
        required: true
      },
      date: {
        required: true,
        date: true
      }
    },
    messages: {
      name: {
        required: "Enter name of expense, please!",
        minlength: "Use at least three characters, please!"
      },
      amount: {
        required: "Enter amount of expense, please!",
      },
      description: {
        required: "Enter description of expense, please!",
      },
      comment: {
        required: "Enter comment of expense, please!",
      },
      date: {
        required: "Enter date of expense, please!",
        date: "Field should be date!",
      }
    },
    submitHandler() { _handleEdit( template ); }
  };
};

let _handleEdit = ( template ) => {
    var id = FlowRouter.getParam('id');
    check( id, String );

    var expense = {
      name: template.find( '[name="name"]' ).value,
      description: template.find( '[name="description"]' ).value,
      amount: template.find( '[name="amount"]' ).value,
      comment: template.find( '[name="comment"]' ).value,
      date: new Date(template.find( '[name="date"]' ).value + " GMT")
    };

    Meteor.call('updateExpense', id, expense, ( error ) => {
        if ( error ) {
            Bert.alert( error.reason, 'danger' );
        } else {
            Bert.alert( 'Expense edited!', 'success' );
        }
    });
};

Modules.client.editExpense = editExpense;
