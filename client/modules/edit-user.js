let editUser = ( options ) => {
  _validate( options.form, options.template );
};

let _validate = ( form, template ) => {
  $( form ).validate( validation( template ) );
};

let validation = ( template ) => {
  return {
    rules: {
      name: {
        required: true,
        minlength: 3
      },
      email: {
        required: true,
        email: true
      },
      password: {
        minlength: 6
      }
    },

    messages: {
      name: {
        required: "Enter name, please!",
        minlength: "Use at least three characters, please!"
      },
      email: {
        required: "Please enter valid email address!",
      },
      newPassword: {
        required: "Enter a password, please.",
        minlength: "Use at least six characters, please."
      }
    },
    submitHandler() { _handleEdit( template ); }
  };
};

let _handleEdit = ( template ) => {
    var id = FlowRouter.getParam('id');
    check( id, String );

    var user = {
      profile: {
        name: {
          first: template.find( '[name="first"]' ).value,
          last: template.find( '[name="last"]' ).value,
        }
      },
      email: template.find( '[name="email"]' ).value,
      password: template.find( '[name="newPassword"]' ).value,
    };

    Meteor.call('updateUser', id, user, ( error ) => {
        if ( error ) {
            Bert.alert( error.reason, 'danger' );
        } else {
            Bert.alert( 'User edited!', 'success' );
        }
    });
};

Modules.client.editUser = editUser;
