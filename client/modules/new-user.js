let newUser = ( options ) => {
  _validate( options.form, options.template );
};

let _validate = ( form, template ) => {
  $( form ).validate( validation( template ) );
};

let validation = ( template ) => {
  return {
    rules: {
      name: {
        required: true,
        minlength: 3
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6
      },
      repeatPassword: {
        required: true,
        minlength: 6,
        equalTo: '[name="password"]'
      }
    },
    messages: {
      name: {
        required: "Enter name, please!",
        minlength: "Use at least three characters, please!"
      },
      email: {
        required: "Please enter valid email address!",
      },
      password: {
        required: "Enter a password, please.",
        minlength: "Use at least six characters, please."
      },
      repeatPassword: {
        required: "Repeat your password, please.",
        equalTo: "Hmm, your passwords don't match. Try again?"
      }
    },
    submitHandler() { _handleNew( template ); }
  };
};

let _handleNew = ( template ) => {

    var user = {
      profile: {
        name: {
          first: template.find( '[name="first"]' ).value,
          last: template.find( '[name="last"]' ).value,
        }
      },
      email: template.find( '[name="email"]' ).value,
      password: template.find( '[name="password"]' ).value,
    };

    Meteor.call('insertUser', user, ( error, response ) => {
        if ( error ) {
            Bert.alert( error.reason, 'danger' );
        } else {
            FlowRouter.go( 'users' );
            Bert.alert( 'User created!', 'success' );
        }
    });
};

Modules.client.newUser = newUser;
