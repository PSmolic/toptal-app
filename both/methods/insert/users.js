Meteor.methods({
  insertUser(options) {
    var role = options.role || 'user';
    check(options, Object);
    check(role, String);

    if (Meteor.users.findOne( { 'emails.address': options.email } )) {
      throw new Meteor.Error(403, "Email address is already registered");
    }

    var userId = Accounts.createUser({
      email: options.email,
      password: options.password,
      profile: options.profile || {}
    });

    Roles.setUserRoles(userId, [role]);

    return userId;
  }
});
