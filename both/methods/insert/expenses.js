Meteor.methods({
  insertExpense(expense) {
    check(expense, Object);

    expense.created = new Date();

    return Expenses.insert(expense);
  }
});
