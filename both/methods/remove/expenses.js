Meteor.methods({
  removeExpense( expenseId ) {
    check( expenseId, String );

    return Expenses.remove( expenseId );
  }
});