Meteor.methods({
  removeUser( userId ) {
    check( userId, String );

    Meteor.users.remove( userId );
  }
});