Meteor.methods({
  updateMethod( argument ) {
    check( argument, Object );

    var documentId = Collection.update( argument._id, {
      $set: { 'key': argument.key }
    });
    
    return documentId;
  }
});
