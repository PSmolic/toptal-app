Meteor.methods({
  updateExpense(id, expense) {
    check(expense, Object);
    check(id, String);

    return Expenses.update(id, {
      $set: expense
    });
  }
});
