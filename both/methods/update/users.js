Meteor.methods({
  updateUser(id, user) {
    check(user, Object);
    check(id, String);

    if (user.password == '') {
      delete user.password;
    }

    if (user.password) {
      Accounts.setPassword(id, user.password);
      delete user.password;
    }

    if (user.role) {
      Roles.setUserRoles( id, [ user.role ] );
      delete user.role;
    }

    if (user.email) {
      Accounts.addEmail(id, user.email)

      var curUser = Meteor.users.findOne({_id: id});
      if(curUser.emails[0].address != user.email) {
        Accounts.removeEmail(id, curUser.emails[0].address);
      }
      delete user.email;
    }

    return Meteor.users.update(id, {
      $set: user
    });
  },
  setRoleOnUser( options ) {
    check( options, {
      user: String,
      role: String
    });

    Roles.setUserRoles( options.user, [ options.role ] );
  }
});
