Meteor.publish("expenses", function () {

  var currentUserId = this.userId;
  let isAdmin = Roles.userIsInRole( currentUserId, 'admin' );

  if(isAdmin) {
    return Expenses.find({});
  }

  return Expenses.find({_userId: currentUserId});
});
