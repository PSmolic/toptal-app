Meteor.publish( 'users', function() {
  let isAdmin = Roles.userIsInRole( this.userId, [ 'admin', 'manager' ] );

  if ( isAdmin ) {

    return [
      Meteor.users.find( {} )
    ];
  } else {

    return null;
  }
});
