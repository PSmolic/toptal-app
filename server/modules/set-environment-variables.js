let setEnvironmentVariables = () => {
  process.env.MAIL_URL = Meteor.settings.private.MAIL_URL;

  Meteor.absoluteUrl.defaultOptions.rootUrl = Meteor.settings.public.ROOT_URL;
};

Modules.server.setEnvironmentVariables = setEnvironmentVariables;
