let generateExpenses = () => {
  let fakeExpenseCount = 50,
      expenseExist    = _checkIfExpenseExist( fakeExpenseCount );

  if ( !expenseExist ) {
    _createExpenses( _generateFakeExpenses( fakeExpenseCount ) );
  }
};

let _checkIfExpenseExist = ( count ) => {

  let expenseCount = Expenses.find().count();
  return expenseCount < count ? false : true;
};

let _createExpenses = ( expenses ) => {
  for ( let i = 0; i < expenses.length; i++ ) {
    let expense       = expenses[ i ],
        expenseExists = _checkIfExpenseExists( expense.name );

    if ( !expenseExists ) {
      _createExpense( expense );
    }
  }
};

let _checkIfExpenseExists = ( name ) => {
  return Expenses.find( { 'name': name } ).count();
};

let _createExpense = ( expense ) => {
  Expenses.insert(expense);
};

let _generateFakeExpenses = ( count ) => {
  let expenses = [];

  for ( let i = 0; i < count; i++ ) {
    expenses.push({
      name: faker.company.companyName(),
      _userId: Meteor.users.find({}, {skip: Math.random() * 5, limit: 1}).fetch()[0]._id,
      description: faker.lorem.sentence(),
      amount: faker.random.number(),
      comment: faker.lorem.sentences(),
      date: faker.date.between(new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 21), new Date()).toGMTString(),
      created: new Date().toGMTString()
    });
  }

  return expenses;
};

Modules.server.generateExpenses = generateExpenses;