Meteor.methods({
  groupedByWeek: function () {

    var query = [
      { $match: { _userId: Meteor.userId() } },
      { $group: {
          _id : { $week: "$date" },
          Data: {$push: "$$ROOT"},
          totalAmount: { $sum: "$amount" }
        }
      },
      {$sort: {date: -1}}
    ];

    check(query, Array);

    return Expenses.aggregate(query);
  },
   groupedByDay: function (weekNum) {
    check(weekNum, Number);

    var weekStart = moment().hour(0).minute(0).day("Sunday").week(weekNum + 1);
    var weekEnd = moment().hour(23).minute(59).day("Saturday").week(weekNum + 1);

    var query = [
      { $match: { _userId: Meteor.userId(), date: {$gte: weekStart.toDate(), $lte: weekEnd.toDate()  } } },
      { $group: {
          _id : { $dayOfWeek: "$date" },
          //_id : { week: { $week: "$date" }, day: { $dayOfWeek: "$date" }, year: { $year: "$date" } },
          Data: {$push: "$$ROOT"},
          totalAmount: { $sum: "$amount" },
          averageAmount: { $avg: "$amount" },
        }
      },
      {$sort: {date: -1}}
    ];

    return Expenses.aggregate(query);
  }
})
