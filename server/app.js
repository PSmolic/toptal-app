var Api = new Restivus({
  useDefaultAuth: true,
  prettyJson: true
});

Api.addCollection(Expenses, {
  routeOptions: {
    authRequired: true
  },
  endpoints: {
    getAll: {
      action: function () {
        var currentUserId = this.userId;
        let isAdmin = Roles.userIsInRole( currentUserId, 'admin' );

        var getAll  = this.queryParams;

        if(isAdmin && getAll.findAll === 'true') {
            return Expenses.find().fetch();
        }

        return Expenses.find({_userId: currentUserId}).fetch();
      }
    },
    put: {
      action: function() {

        var exp = Expenses.find({_id: this.urlParams.id}).fetch();

        if(exp._userId !== this.userId && !Roles.userIsInRole( this.userId, [ 'admin' ] )) {
          return {
            statusCode: 403,
            body: {status: "fail, You don't have permission to do this"}
          };
        }

        if(Meteor.call('updateExpense', this.urlParams.id, this.bodyParams)) {
          return Expenses.find({_id: this.urlParams.id}).fetch();
        } else {
          return {
            statusCode: 404,
            body: {status: 'fail, Expense not updated'}
          };
        }
      }
    },
    post: {
      action: function() {
        if(!Roles.userIsInRole( this.userId, [ 'admin' ] )) {
          this.bodyParams._userId = this.userId;
        }

        var newExpense = Meteor.call('insertExpense', this.bodyParams);
        if(newExpense) {
          return Expenses.find({_id: newExpense}).fetch();
        } else {
          return {
            statusCode: 404,
            body: {status: 'fail, Expenses not created'}
          };
        }
      }
    },
    delete: {
      action: function() {
        var exp = Expenses.find({_id: this.urlParams.id}).fetch();

        if(exp._userId === this.userId || Roles.userIsInRole( this.userId, [ 'admin' ] )) {
          if(Meteor.call('removeExpense', this.urlParams.id)) {
            return {
            statusCode: 200,
              body: {status: 'Success'}
            };
          }
        }
        return {
          statusCode: 403,
          body: {status: "fail, You don't have permission to do this"}
        };
      }
    }
  }
});

Api.addCollection(Meteor.users, {
  routeOptions: {
    authRequired: true
  },
  endpoints: {
    getAll: {
      action: function () {
        var currentUserId = this.userId;
        let isAdmin = Roles.userIsInRole( currentUserId, 'admin' );

        if(isAdmin) {
            return Meteor.users.find().fetch();
        } else {
          return {
            statusCode: 404,
            body: {status: 'fail, Users not found'}
          };
        }
      }
    },
    delete: {
      roleRequired: 'admin,manager'
    },
    put: {
      action: function() {

        if(!Roles.userIsInRole( this.userId, [ 'admin', 'manager' ] )  && this.userId != this.urlParams.id) {
          return {
            statusCode: 403,
            body: {status: "fail, You don't have permission to do this"}
          };
        }

        if(Meteor.call('updateUser', this.urlParams.id, this.bodyParams)) {
          return Meteor.users.find({_id: this.urlParams.id}).fetch();
        } else {
          return {
            statusCode: 404,
            body: {status: 'fail, Users not updated'}
          };
        }
      }
    },
    post: {
      action: function() {
        if(!Roles.userIsInRole( this.userId, [ 'admin', 'manager' ] )) {
          this.bodyParams.role = 'user';
        }

        var newUser = Meteor.call('insertUser', this.bodyParams);
        if(newUser) {
          return Meteor.users.find({_id: newUser}).fetch();
        } else {
          return {
            statusCode: 404,
            body: {status: 'fail, Users not created'}
          };
        }
      }
    }
  }
});
